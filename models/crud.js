var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var crudSchema = new Schema({
  firstname:  String,
  secondname: String,
  email:   String,
  password: String,
  confirmpassword: String,
  userimage: String
  
});

module.exports = mongoose.model('Crud', crudSchema);