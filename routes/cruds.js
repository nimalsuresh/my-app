var express = require('express');
var router = express.Router();
var Crud = require('../models/crud');

var multer = require('multer');
var upload = multer({ dest: '' })


router.post('/adduser', function(req, res, next)
{
  var crud = new Crud({ firstname: req.body.firstname , secondname: req.body.secondname , email: req.body.email , password: req.body.password });
  crud.save(function (err, docs) 
 {
  if (err) return handleError(err);
  res.send('Success');
 });

});


router.get('/getallusers', function(req, res, next)
{
  Crud.find({}, function (err, docs) 
  {
    res.json({status: 'success',  records: docs});

    });
});


router.post('/deleteuser', function(req, res, next)
{
  
  Crud.findOneAndDelete({firstname:req.body.firstname}, function(err, doc){
    if(err){
      res.send('error occured')
    }else
    {
      res.send('deleted')
    }
  })
})


router.post('/updateuser' , function(req, res, next)
{
  Crud.findOneAndUpdate({secondname:req.body.secondname},{$set:{secondname:'nimal'}} , function(err, doc){
    if(err){
      res.send('error occured')
    }else{
      res.send('updated')
    }
 })
})


router.post('/finduser',function(req,res,next){
  Crud.findOne({email:req.body.email, password:req.body.password}, function(err,docs){
    if (err) {
      res.status(400).send(err);
     } else if (!docs) {
      res.status(500).send(err);
     } else {
      res.status(200).send(docs);
     }  })
})
 

  router.post('/imageupload', function (req, res, next) {
    upload(req, res, function (err) {
      if (err instanceof multer.MulterError) {
        // A Multer error occurred when uploading.
      } else if (err) {
        // An unknown error occurred when uploading.
      }
   res.status(200).send(docs)
      // Everything went fine.
    })
  })
  

module.exports = router;