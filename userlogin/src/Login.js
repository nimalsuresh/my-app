import React ,{Component} from 'react';
import { Form,FormControl,FormGroup,FormLabel,Button,Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import axios from 'axios';
import './App.css';

class App extends Component {
    constructor(props){
        super(props)
        this.state = { email:'', password:'' }

         this.userFound = this.userFound.bind(this)
this.submitChange = this.submitChange.bind(this)
    }

submitChange(e)
{

this.setState({[e.target.name]:event.target.value})
this.setState({[e.target.name]:event.target.value})
}

userFound(){
  var self = this;
  axios.post('/cruds/finduser',
  {email: this.state.email,
   password: this.state.password})
  .then( function(response) 
  {
    self.state.cruds = response.data
  })
  .catch(function(error){
    console.log(error)
  })
self.forceUpdate()
}


render()
{
return (
  <div align="center">
    <h1><b>My Website</b></h1>

<Form>
<FormGroup as={Col} md="2" controlId="formBasicEmail">
<FormLabel>Email address</FormLabel>
<FormControl type="email" name="email" value={this.state.email} onChange={e =>this.submitChange(e)} placeholder="Enter email" />
</FormGroup>

<FormGroup as={Col} md="2" controlId="formBasicPassword">
<FormLabel>Password</FormLabel>
<FormControl type="password" name="password" value={this.state.password} onChange={e =>this.submitChange(e)} placeholder="Password" />
</FormGroup>

<Button variant="primary" className="btn-dark btn-block" as={Col} md="1" onClick={this.userFound} >LOGIN</Button>

       <Link to="/Register">Register</Link>
       </Form>

</div>
)}

}

export default App;
