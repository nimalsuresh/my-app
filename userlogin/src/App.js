import React, { Component } from 'react';
import './App.css';
import Register from './Register';
import Login from './Login';
import {  BrowserRouter as Router,Switch,Route} from "react-router-dom";
 
      
      class App extends Component 
      {
        render()
         {
          return (
            <Router>
              <Switch>
                <Route exact path="/Register" component={Register} />
                <Route path="/" component={Login} />
             </Switch>
            </Router>
       
          );
         }
      }
      
      export default App;