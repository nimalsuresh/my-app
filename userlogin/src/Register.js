import React ,{Component} from 'react';
import { Form,FormControl,FormGroup,FormLabel,Button,Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import axios from 'axios';
import './App.css';


class App extends Component
{
    constructor(props)
    {
    super(props)

    this.state = { data:[], firstname:'', secondname:'', email:'', password:'' }
    
    this.createOne= this.createOne.bind(this)
    this.deleteOne= this.deleteOne.bind(this)
    this.updateOne= this.updateOne.bind(this)
    this.onChangeName = this.onChangeName.bind(this)
    
    }
    onChangeName(e)
    {
      this.setState({[e.target.name]:event.target.value})
      this.setState({[e.target.name]:event.target.value})
      this.setState({[e.target.name]:event.target.value})
      this.setState({[e.target.name]:event.target.value})
    }
    

    createOne()
  {
    var self = this
    axios.post('/cruds/adduser',
    {
      firstname: this.state.firstname,
     secondname: this.state.secondname,
     email: this.state.email,
     password: this.state.password  })

    .then( function(response) 
    {
      self.state.cruds = response.data
    })
    .catch(function(error){
      console.log(error)
    })
    this.forceUpdate()
  }


  deleteOne()
{
  //alert('delete')
  var self = this
  axios.post('/cruds/deleteuser',{

    firstname: this.state.firstname
     
   })
 .then(function (response) {
   // handle success
   self.state.cruds = response.data
   //self.forceUpdate()
 })
 .catch(function (error) {
   // handle error
   console.log(error);
 })
}


updateOne()
{
  //alert('delete')
  var self = this
  axios.post('/cruds/updateuser',
  {secondname: this.state.secondname})
 .then(function (response) {
   // handle success
   self.state.cruds = response.data
   //self.forceUpdate()
 })
 .catch(function (error) {
   // handle error
   console.log(error);
 })
}

render() 
   {
    return (
      <div align="center">
        <h1><b>My Website</b></h1>
    <Form >
 
    <FormGroup as={Col} md="2" controlId = "formGroupFirstname">
    <FormLabel>FirstName</FormLabel>
    <FormControl type="text"  name="firstname" value={this.state.firstname} onChange={e =>this.onChangeName(e)} placeholder = "firstname" />
    </FormGroup>

    <FormGroup as={Col} md="2" controlId="formGroupSecondname">
    <FormLabel>Secondname</FormLabel>
    <FormControl type="text" name="secondname" value={this.state.secondname} onChange={e =>this.onChangeName(e)} placeholder="secondname"/>
    </FormGroup>

    <FormGroup as={Col} md="2" controlId="formGroupEmail">
    <FormLabel>Email</FormLabel>
    <FormControl type="email" name="email" value={this.state.email} onChange={e =>this.onChangeName(e)} placeholder="enter email"/>
    </FormGroup> 


    <FormGroup as={Col} md="2" controlId="formGroupPassword">
    <FormLabel>Password</FormLabel>
    <FormControl type="password"  name="password" value={this.state.password} onChange={e =>this.onChangeName(e)} placeholder="password"/>
    </FormGroup>

    <FormGroup as={Col} md="2" controlId="formGroupConfirmPassword">
    <FormLabel>ConfirmPassword</FormLabel>
    <FormControl type="password" placeholder="confirm password"/>
    </FormGroup>
    

 <Button className="btn-dark btn-block" as={Col} md="1" type="submit" onClick={this.createOne}>Create</Button>
 <Button className="btn-dark btn-block" as={Col} md="1" type="submit" onClick={this.deleteOne}>delete</Button>
 <Button className="btn-dark btn-block" as={Col} md="1" type="submit" onClick={this.updateOne}>update</Button>

 <Link to="/Login">LOGIN</Link>
</Form>

</div>
 );
}
}

export default App;


